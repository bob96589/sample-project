package com.sample.dao;

import java.util.List;
import java.util.Map;

import com.sample.model.Topic;

public interface TopicDao {
    public boolean saveOrUpdate(Topic topic);

    public List<Topic> list(int firstResult, int maxResult);

    public boolean delete(String oid);

    public int getTotalCnt();

    public int getNextTopno();

	public List<Map<String, Object>> listNativeSQL(int firstResult, int maxResult);

	public Topic findByOid(String topicOid);
}
