package com.sample.dao;

import java.util.List;

import com.sample.model.Article;

public interface ArticleDao {
    public boolean saveOrUpdate(Article article);

    public List<Article> list(int firstResult, int maxResult);

    public boolean delete(String oid);

    public int getTotalCnt();

    public int getNextArtno();

	public List<Article> findByPid(String oid, int firstResult, int maxResult);

	public int getTotalCntByPid(String oid);

	public void delete(List<Article> articleList);

	public List<Article> findByPid(String pid);
}
