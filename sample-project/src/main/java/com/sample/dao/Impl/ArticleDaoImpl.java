package com.sample.dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sample.dao.ArticleDao;
import com.sample.model.Article;

@Repository
public class ArticleDaoImpl implements ArticleDao {

    @Autowired
    SessionFactory session;

    public boolean saveOrUpdate(Article article) {
        session.getCurrentSession().persist(article);
        return true;
    }

    public List<Article> list(int firstResult, int maxResult) {
        Query query = session.getCurrentSession().createQuery("FROM Article A ORDER BY A.artasktime DESC");
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        List<Article> list = query.list();
        return list;
    }

    public boolean delete(String oid) {
        try {
        	Article article = (Article) session.getCurrentSession().get(Article.class, oid);
            session.getCurrentSession().delete(article);
        } catch (Exception ex) {
            return false;
        }

        return true;
    }

    @Override
    public int getTotalCnt() {
        Query countQuery = session.getCurrentSession().createQuery("SELECT COUNT(A.oid) FROM Article A");
        Long totalCnt = (Long) countQuery.uniqueResult();
        return totalCnt.intValue();
    }

    @Override
    public int getNextArtno() {
        Query countQuery = session.getCurrentSession().createQuery("SELECT MAX(A.artno) FROM Article A");
        int max;
        if(countQuery.uniqueResult() == null){
        	max = 0;
        }else{
        	max = (int) countQuery.uniqueResult();
        }
        return max + 1;
    }

	@Override
	public List<Article> findByPid(String oid, int firstResult, int maxResult) {
		Query query = session.getCurrentSession().createQuery("FROM Article A WHERE A.pid = :pid ORDER BY A.artasktime DESC");
		query.setParameter("pid", oid);
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        List<Article> list = query.list();
		return list;
	}

	@Override
	public int getTotalCntByPid(String oid) {
        Query countQuery = session.getCurrentSession().createQuery("SELECT COUNT(A.oid) FROM Article A WHERE A.pid = :pid");
        countQuery.setParameter("pid", oid);
        Long totalCnt = (Long) countQuery.uniqueResult();
        return totalCnt.intValue();
	}

	@Override
	public void delete(List<Article> articleList) {
		Session s = session.getCurrentSession();
		for (Article article : articleList) {
			s.delete(article);
		}
	}

	@Override
	public List<Article> findByPid(String pid) {
	    Query query = session.getCurrentSession().createQuery("FROM Article A WHERE A.pid = :pid");
	    query.setParameter("pid", pid);
	    List<Article> list = query.list();
		return list;
	}

}
