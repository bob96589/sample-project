package com.sample.dao.Impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sample.dao.TopicDao;
import com.sample.model.Topic;

@Repository
public class TopicDaoImpl implements TopicDao {

	@Autowired
	SessionFactory session;

	public boolean saveOrUpdate(Topic topic) {
		session.getCurrentSession().persist(topic);
		return true;
	}

	public List<Topic> list(int firstResult, int maxResult) {
		Query query = session.getCurrentSession().createQuery("FROM Topic T ORDER BY T.toptime DESC");
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);
		List<Topic> list = query.list();
		return list;
	}

	@Override
	public List<Map<String, Object>> listNativeSQL(int firstResult, int maxResult) {
		String sql = "SELECT OID, TOPNO, FORNO, MEMNO, TOPNAME, TOPTIME, TOPSTATUS, CASE WHEN ISNULL(CNT,'') ='' THEN  0 ELSE CNT END CNT FROM TOPIC T LEFT JOIN ( SELECT PID, COUNT(PID) CNT FROM ARTICLE GROUP BY PID ) A ON T.OID = A.PID ORDER BY T.TOPTIME DESC";
		SQLQuery query = session.getCurrentSession().createSQLQuery(sql);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);
		List<Object[]> list = query.list();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		for (Object[] objAry : list) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oid", objAry[0]);
			map.put("topno", objAry[1]);
			map.put("forno", objAry[2]);
			map.put("memno", objAry[3]);
			map.put("topname", objAry[4]);
			map.put("toptime", objAry[5]);
			map.put("topstatus", objAry[6]);
			map.put("cnt", objAry[7]);
			resultList.add(map);
		}
		return resultList;
	}

	public boolean delete(String oid) {
		try {
			Topic topic = findByOid(oid);
			session.getCurrentSession().delete(topic);
		} catch (Exception ex) {
			return false;
		}

		return true;
	}

	@Override
	public int getTotalCnt() {
		Query countQuery = session.getCurrentSession().createQuery("SELECT COUNT(T.oid) FROM Topic T");
		Long totalCnt = (Long) countQuery.uniqueResult();
		return totalCnt.intValue();
	}

	@Override
	public int getNextTopno() {
		Query countQuery = session.getCurrentSession().createQuery("SELECT MAX(T.topno) FROM Topic T");
		int max = (int) countQuery.uniqueResult();
		return max + 1;
	}

	@Override
	public Topic findByOid(String topicOid) {
		Topic topic = (Topic) session.getCurrentSession().get(Topic.class, topicOid);
		return topic;
	}

}
