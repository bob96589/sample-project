package com.sample.utils;

import java.util.UUID;

public class Utils {

    public static String getUUID() {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        return uuid;
    }

}
