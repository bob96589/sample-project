package com.sample.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sample.service.ForumService;

@Controller
@RequestMapping("addArticle")
public class AddArticleController {

    @Autowired
    ForumService forumService;

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public ModelAndView getPage(@RequestParam String topicOid) {
        ModelAndView view = new ModelAndView("addArticle");
        view.addObject("topicOid", topicOid);
        return view;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> addArticle(@RequestParam String topicOid, @RequestParam String artcon) {
        Map<String, Object> map = new HashMap<String, Object>();
        forumService.createArticle(topicOid, artcon);
        map.put("NOTIFY_MESSAGE", "Your record have been saved successfully");
        return map;
    }

}
