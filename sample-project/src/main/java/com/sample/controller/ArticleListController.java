package com.sample.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sample.model.Topic;
import com.sample.service.ForumService;

@Controller
@RequestMapping("articleList")
public class ArticleListController {

    @Autowired
    ForumService forumService;

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public ModelAndView getPage(@RequestParam String topicOid) {
        ModelAndView view = new ModelAndView("articleList");
        Topic topic = forumService.findTopicByOid(topicOid);
        view.addObject("topicOid", topicOid);
        view.addObject("topname", topic.getTopname());
        return view;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> getAll(@RequestParam String oid, @RequestParam int firstResult, @RequestParam int maxResult) {
        Map<String, Object> pageMap = forumService.loadArticlePage(oid, firstResult, maxResult);
        return pageMap;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> delete(@RequestParam String articleOid) {
        Map<String, Object> map = new HashMap<String, Object>();
        forumService.deleteArticle(articleOid);
        return map;
    }
}
