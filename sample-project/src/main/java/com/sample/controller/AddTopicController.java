package com.sample.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sample.service.ForumService;

@Controller
@RequestMapping("addTopic")
public class AddTopicController {

    @Autowired
    ForumService forumService;

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public ModelAndView getPage() {
        ModelAndView view = new ModelAndView("addTopic");
        return view;
    }

    @RequestMapping(value = "/addTopic", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> addTopic(@RequestParam String topname, @RequestParam String artcon) {
        Map<String, Object> map = new HashMap<String, Object>();
        String topicOid = forumService.createTopic(topname, artcon);
        map.put("topicOid", topicOid);
        map.put("NOTIFY_MESSAGE", "Your record have been saved successfully");
        return map;
    }

}
