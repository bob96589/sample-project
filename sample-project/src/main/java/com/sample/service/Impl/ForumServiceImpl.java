package com.sample.service.Impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sample.dao.ArticleDao;
import com.sample.dao.TopicDao;
import com.sample.model.Article;
import com.sample.model.Topic;
import com.sample.service.ForumService;
import com.sample.utils.Utils;

@Service
public class ForumServiceImpl implements ForumService {

	@Autowired
	TopicDao topicDao;
	@Autowired
	ArticleDao articleDao;

	public boolean saveOrUpdate(Topic topic) {
		return topicDao.saveOrUpdate(topic);
	}

	public List<Topic> list(int firstResult, int maxResult) {
		return topicDao.list(firstResult, maxResult);
	}

	public boolean deleteTopic(String oid) {
		List<Article> articleList = articleDao.findByPid(oid);
		articleDao.delete(articleList);
		
		return topicDao.delete(oid);
	}

	@Override
	public String createTopic(String topname, String artcon) {
		String topicOid = Utils.getUUID();
		Date current = new Date();
		int topno = topicDao.getNextTopno();
		int memno = 90001;
		int artno = articleDao.getNextArtno();

		Topic topic = new Topic();
		topic.setOid(topicOid);
		topic.setForno(150001);
		topic.setMemno(memno);
		topic.setTopname(topname);
		topic.setTopno(topno);
		topic.setTopstatus(0);
		topic.setToptime(current);
		topicDao.saveOrUpdate(topic);

		Article article = new Article();
		article.setOid(Utils.getUUID());
		article.setPid(topicOid);
		article.setTopno(topno);
		article.setMemno(memno);
		article.setArtasktime(current);
		article.setArtcon(artcon);
		article.setArtno(artno);
		article.setArtstate(0);
		articleDao.saveOrUpdate(article);
        return topicOid;

	}

	@Override
	public Map<String, Object> loadTopicPage(int firstResult, int maxResult) {
		List<Map<String, Object>> partList = topicDao.listNativeSQL(firstResult, maxResult);
		// List<Topic> partList = topicDao.list(firstResult, maxResult);
		int totalCnt = topicDao.getTotalCnt();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", partList);
		map.put("totalCnt", totalCnt);
		map.put("totalPage", (totalCnt / maxResult) + (totalCnt % maxResult == 0 ? 0 : 1));
		return map;
	}

	@Override
	public Map<String, Object> loadArticlePage(String oid, int firstResult, int maxResult) {
		List<Article> partList = articleDao.findByPid(oid, firstResult, maxResult);
		int totalCnt = articleDao.getTotalCntByPid(oid);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", partList);
		map.put("totalCnt", totalCnt);
		map.put("totalPage", (totalCnt / maxResult) + (totalCnt % maxResult == 0 ? 0 : 1));
		return map;
	}

	@Override
	public boolean deleteArticle(String oid) {
		return articleDao.delete(oid);
	}

	@Override
	public void createArticle(String topicOid, String artcon) {
		Topic topic = topicDao.findByOid(topicOid);

		Article article = new Article();
		article.setOid(Utils.getUUID());
		article.setPid(topicOid);
		article.setTopno(topic.getTopno());
		article.setMemno(90002);
		article.setArtasktime(new Date());
		article.setArtcon(artcon);
		article.setArtno(articleDao.getNextArtno());
		article.setArtstate(0);
		articleDao.saveOrUpdate(article);

	}

	@Override
	public Topic findTopicByOid(String topicOid) {
		return topicDao.findByOid(topicOid);
	}

}
