package com.sample.service;

import java.util.Map;

import com.sample.model.Topic;

public interface ForumService {
    public boolean saveOrUpdate(Topic topic);

    public boolean deleteTopic(String oid);

    public String createTopic(String topname, String artcon);

    public Map<String, Object> loadTopicPage(int firstResult, int maxResult);

	public Map<String, Object> loadArticlePage(String oid, int firstResult, int maxResult);

	public boolean deleteArticle(String oid);

	public void createArticle(String topicOid, String artcon);

	public Topic findTopicByOid(String topicOid);
}
