package com.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TOPIC")
public class Topic {

    @Id
    @Column(unique = true, nullable = false, length = 32, columnDefinition = "CHAR(32)")
    private String oid;

    @Column(name = "TOPNO")
    private Integer topno;

    @Column(name = "FORNO")
    private Integer forno;

    @Column(name = "MEMNO")
    private Integer memno;

    @Column(name = "TOPNAME")
    private String topname;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TOPTIME")
    private Date toptime;

    @Column(name = "TOPSTATUS")
    private Integer topstatus;

    public Integer getTopstatus() {
        return topstatus;
    }

    public void setTopstatus(Integer topstatus) {
        this.topstatus = topstatus;
    }

    public Integer getTopno() {
        return topno;
    }

    public void setTopno(Integer topno) {
        this.topno = topno;
    }

    public Integer getForno() {
        return forno;
    }

    public void setForno(Integer forno) {
        this.forno = forno;
    }

    public Integer getMemno() {
        return memno;
    }

    public void setMemno(Integer memno) {
        this.memno = memno;
    }

    public String getTopname() {
        return topname;
    }

    public void setTopname(String topname) {
        this.topname = topname;
    }

    public Date getToptime() {
        return toptime;
    }

    public void setToptime(Date toptime) {
        this.toptime = toptime;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @Override
    public String toString() {
        return "Topic [oid=" + oid + ", topno=" + topno + ", forno=" + forno + ", memno=" + memno + ", topname=" + topname + ", toptime=" + toptime + ", topstatus=" + topstatus + "]";
    }

}
