package com.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ARTICLE")
public class Article {

	@Id
	@Column(unique = true, nullable = false, length = 32, columnDefinition = "CHAR(32)")
	private String oid;

	@Column(name = "PID")
	private String pid;

	@Column(name = "ARTNO")
	private Integer artno;

	@Column(name = "MEMNO")
	private Integer memno;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ARTASKTIME")
	private Date artasktime;

	@Column(name = "ARTCON")
	private String artcon;

	@Column(name = "TOPNO")
	private Integer topno;

	@Column(name = "ARTSTATE")
	private Integer artstate;

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Integer getArtno() {
		return artno;
	}

	public void setArtno(Integer artno) {
		this.artno = artno;
	}

	public Integer getMemno() {
		return memno;
	}

	public void setMemno(Integer memno) {
		this.memno = memno;
	}

	public Date getArtasktime() {
		return artasktime;
	}

	public void setArtasktime(Date artasktime) {
		this.artasktime = artasktime;
	}

	public String getArtcon() {
		return artcon;
	}

	public void setArtcon(String artcon) {
		this.artcon = artcon;
	}

	public Integer getTopno() {
		return topno;
	}

	public void setTopno(Integer topno) {
		this.topno = topno;
	}

	public Integer getArtstate() {
		return artstate;
	}

	public void setArtstate(Integer artstate) {
		this.artstate = artstate;
	}

}
