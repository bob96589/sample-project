$(document).ready(function() {

  // grid
  delete_ = function(oid) {
	confirm("Are you sure you want to delete?") && $.ajax({
      url : 'delete',
      type : 'POST',
      data : {
        oid : oid
      },
      success : function(json) {
        load(parseInt($("#pageNo").text()));
      }
    });
  }
  function load(pageNo) {
    var maxResult = 10;
    $.ajax({
      url : 'list',
      type : 'POST',
      data : {
        firstResult : (pageNo - 1) * maxResult,
        maxResult : maxResult
      },
      success : function(json) {
        $('.tr').remove();
        for (i = 0; i < json.data.length; i++) {
          var str = "<tr class='tr'> <td> " + json.data[i].oid + " </td>";
          str += "<td> " + json.data[i].topno + " </td>";
          str += "<td> " + json.data[i].forno + " </td>";
          str += "<td> " + json.data[i].memno + " </td>";
          str += "<td> <a href=" + url('mvc/articleList/page') + "?topicOid=" + json.data[i].oid + ">" + json.data[i].topname + "</a> </td>";
          str += "<td> " + json.data[i].toptime + " </td>";
          str += "<td> " + json.data[i].topstatus + " </td>";
          str += "<td> " + json.data[i].cnt + " </td>";
          str += "<td> <a href='#' onclick='delete_(\"" + json.data[i].oid + "\");'> Delete </a>  </td> </tr>";
          $("#table").append(str);
        }
        $("#totalCnt").text(json.totalCnt);
        $("#totalPage").text(json.totalPage);
        $("#pageNo").text(pageNo);

        var prevFlag = pageNo == 1 ? true : false;
        $("#firstPage").prop('disabled', prevFlag);
        $("#previousPage").prop('disabled', prevFlag);

        var nextFlag = pageNo == json.totalPage ? true : false;
        $("#nextPage").prop('disabled', nextFlag);
        $("#lastPage").prop('disabled', nextFlag);
      }
    });
  }

  // event
  $("#firstPage").click(function() {
    load(1);
  });
  $("#previousPage").click(function() {
    load(parseInt($("#pageNo").text()) - 1);
  });
  $("#nextPage").click(function() {
    load(parseInt($("#pageNo").text()) + 1);
  });
  $("#lastPage").click(function() {
    load(parseInt($("#totalPage").text()));
  });
  $("#addTopic").click(function() {
    window.location.href=url("mvc/addTopic/page");
  });

  //function call
  load(1);

});