$(document).ready(function() {

  var topicOid = $("#topicOid").text();
  // grid
  delete_ = function(articleOid) {
    confirm("Are you sure you want to delete?") && $.ajax({
      url : 'delete',
      type : 'POST',
      data : {
        articleOid : articleOid
      },
      success : function(json) {
        load(parseInt($("#pageNo").text()));
      }
    });
  }
  function load(pageNo) {
    var maxResult = 10;
    $.ajax({
      url : 'list',
      type : 'POST',
      data : {
        firstResult : (pageNo - 1) * maxResult,
        maxResult : maxResult,
        oid : topicOid
      },
      success : function(json) {
        $('.tr').remove();
        for (i = 0; i < json.data.length; i++) {
          var str = "<tr class='tr'> <td> " + json.data[i].oid + " </td>";
          str += "<td> " + json.data[i].pid + " </td>";
          str += "<td> " + json.data[i].artno + " </td>";
          str += "<td> " + json.data[i].memno + " </td>";
          str += "<td> " + json.data[i].artasktime + " </td>";
          str += "<td> " + json.data[i].artcon + " </td>";
          str += "<td> " + json.data[i].topno + " </td>";
          str += "<td> " + json.data[i].artstate + " </td>";
          str += "<td> <a href='#' onclick='delete_(\"" + json.data[i].oid + "\");'> Delete </a>  </td> </tr>";
          $("#table").append(str);
        }
        $("#totalCnt").text(json.totalCnt);
        $("#totalPage").text(json.totalPage);
        $("#pageNo").text(pageNo);

        var prevFlag = pageNo == 1 ? true : false;
        $("#firstPage").prop('disabled', prevFlag);
        $("#previousPage").prop('disabled', prevFlag);

        var nextFlag = pageNo == json.totalPage ? true : false;
        $("#nextPage").prop('disabled', nextFlag);
        $("#lastPage").prop('disabled', nextFlag);
      }
    });
  }

  // event
  $("#firstPage").click(function() {
    load(1);
  });
  $("#previousPage").click(function() {
    load(parseInt($("#pageNo").text()) - 1);
  });
  $("#nextPage").click(function() {
    load(parseInt($("#pageNo").text()) + 1);
  });
  $("#lastPage").click(function() {
    load(parseInt($("#totalPage").text()));
  });
  $("#backToTopicList").click(function() {
    window.location.href = url("mvc/topicList/page");
  });
  $("#reply").click(function() {
    window.location.href = url("mvc/addArticle/page?topicOid=" + topicOid);
  });

  // function call
  load(1);

});