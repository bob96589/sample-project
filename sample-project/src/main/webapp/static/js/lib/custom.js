$.extend(window, {
  contextName : "/sample-project/",
  url : function(route) {
    return contextName + route
  }
});

$(document).ajaxStart(function() {
  //  $.blockUI({ message: '<img src="圖片路徑" />' });
  $.blockUI({
    message : 'loading...'
  });
}).ajaxStop($.unblockUI);

// add jQuery static method
$.extend({
  __ajax : $.ajax,
  ajax : function(s) {
    return $.__ajax($.extend({
      cache : false,
      //dataType : "json",
      //type : 'post'
    }, s, {
      success : function(json, status) {
        if (json.NOTIFY_MESSAGE) {
          alert(json.NOTIFY_MESSAGE);
        }
        s.success && s.success(json, status);
      },
      error : function(xhr, status, e) {
        s.error && s.error(xhr, status, e);
        // var w = window.open();
        // $(w.document.body).html(XMLHttpRequest.responseText);
        // console.error(JSON.stringify(XMLHttpRequest));
        console.error(xhr.responseText);
        alert("Ajax Error: " + xhr.status + " " + xhr.statusText);
      },
    }));
  },
});