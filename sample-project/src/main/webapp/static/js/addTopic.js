$(document).ready(function() {

  // event
  $("#submit").click(function() {
    $.ajax({
      url : 'addTopic',
      type : 'POST',
      data : $("#form").serialize(),
      success : function(json) {
        window.location.href = url("mvc/articleList/page?topicOid=" + json.topicOid);
      }
    });
  });

  $("#cancel").click(function() {
    window.location.href = url("mvc/topicList/page");
  });

});