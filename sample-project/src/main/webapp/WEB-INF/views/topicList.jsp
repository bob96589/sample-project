<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>topicList</title>
<script src="<%=request.getContextPath()%>/static/js/lib/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/static/js/lib/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/static/js/lib/custom.js"></script>
<script src="<%=request.getContextPath()%>/static/js/topicList.js"></script>
</head>
<body>

	<button id="addTopic">Add Topic</button>

	<table id="table" border=1>
		<tr>
			<th>oid</th>
			<th>topno</th>
			<th>forno</th>
			<th>memno</th>
			<th>topname</th>
			<th>toptime</th>
			<th>topstatus</th>
			<th>articleCnt</th>
			<th>Delete</th>
		</tr>
	</table>

	<button id="firstPage">firstPage</button>
	<button id="previousPage">previousPage</button>
	Page No: <span id="pageNo"></span>
	<button id="nextPage">nextPage</button>
	<button id="lastPage">lastPage</button>
	Total Count: <span id="totalCnt"></span> 
	Total Page: <span id="totalPage"></span>

</body>
</html>