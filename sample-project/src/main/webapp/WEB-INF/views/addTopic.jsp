<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>topicList</title>
<script src="<%=request.getContextPath()%>/static/js/lib/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/static/js/lib/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/static/js/lib/custom.js"></script>
<script src="<%=request.getContextPath()%>/static/js/addTopic.js"></script>
</head>
<body>


<form id="form">
	<table>
		<tr>
			<td>Topic Name</td>
			<td><input type="text" id="topname" name="topname"/></td>
		</tr>
		<tr>
			<td>Article</td>
			<td><input type="text" id="artcon" name="artcon"/></td>
		</tr>
	</table>
</form>
<button id="submit">Submit</button>
<button id="cancel">Cancel</button>


</body>
</html>