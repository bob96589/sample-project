<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>articleList</title>
<script src="<%=request.getContextPath()%>/static/js/lib/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/static/js/lib/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/static/js/lib/custom.js"></script>
<script src="<%=request.getContextPath()%>/static/js/articleList.js"></script>
</head>
<body>

	<div id="topicOid">${topicOid}</div>
	Topic Name: <div id="topname">${topname}</div>
	<div>articleList</div>
	<button id="backToTopicList">Back To Topic List</button>
	<button id="reply">reply</button>
	
	<table id="table" border=1>
		<tr>
			<th>oid</th>
			<th>pid</th>
			<th>artno</th>
			<th>memno</th>
			<th>artasktime</th>
			<th>artcon</th>
			<th>topno</th>
			<th>artstate</th>
		</tr>
	</table>

	<button id="firstPage">firstPage</button>
	<button id="previousPage">previousPage</button>
	Page No: <span id="pageNo"></span>
	<button id="nextPage">nextPage</button>
	<button id="lastPage">lastPage</button>
	Total Count: <span id="totalCnt"></span> 
	Total Page: <span id="totalPage"></span>
	

</body>
</html>